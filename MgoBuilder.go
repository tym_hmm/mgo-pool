package MgoPool

var (
	DEFAULT_SESSION_SIZE int32 = 5
	DEFAULT_POOL_SIZE    int32 = 10

)

type mgoBuilder struct {
	builderName  string //构建名称
	addr         string //主机地址  host:port
	user         string //账号
	pwd          string //密码
	dataBase     string //数据库
	authDataBase string //权限数据库

	sessionSize int32 //session连接个数
	//
	poolSize int32 //每个session下的连接池

	isDebug bool   //是否是调试
	logDir  string //日志保存目录
}

/**
构建builder
@param builderName string 构建名称(唯一)
@param addr string 服务地址
@param user string 用户名
@param pwd string 密码
@param dataBase string 数据库
*/
func NewMgoBuilder(builderName string, addr string, user string, pwd string, dataBase string) MgoBuilderApi {
	mgoBuilder := &mgoBuilder{
		builderName: builderName,
		addr:        addr,
		user:        user,
		pwd:         pwd,
		dataBase:    dataBase,
		sessionSize: DEFAULT_SESSION_SIZE,
		poolSize:    DEFAULT_POOL_SIZE,
		isDebug:     true,
	}
	return mgoBuilder
}

func NewMgoBuilderEm() MgoBuilderApi {
	return &mgoBuilder{}
}

func (this *mgoBuilder) SetBuilderName(builderName string) MgoBuilderApi {
	this.builderName = builderName
	return this
}

func (this *mgoBuilder) GetBuilderName() string {
	return this.builderName
}

func (this *mgoBuilder) SetAddr(addr string) MgoBuilderApi {
	this.addr = addr
	return this
}

func (this *mgoBuilder) GetAddr() string {
	return this.addr
}

func (this *mgoBuilder) SetUser(user string) MgoBuilderApi {
	this.user = user
	return this
}

func (this *mgoBuilder) GetUser() string {
	return this.user
}

func (this *mgoBuilder) SetPwd(pwd string) MgoBuilderApi {
	this.pwd = pwd
	return this
}

func (this *mgoBuilder) GetPwd() string {
	return this.pwd
}

func (this *mgoBuilder) SetDataBase(dataBase string) MgoBuilderApi {
	this.dataBase = dataBase
	return this
}

func (this *mgoBuilder) GetDataBase() string {
	return this.dataBase
}

func (this *mgoBuilder) SetAuthDataBase(autDataBase string) MgoBuilderApi {
	this.authDataBase = autDataBase
	return this
}

func (this *mgoBuilder) GetAuthDataBase() string {
	return this.authDataBase
}

func (this *mgoBuilder) SetSessionSize(sessionSize int32) MgoBuilderApi {
	this.sessionSize = sessionSize
	return this
}

func (this *mgoBuilder) GetSessionSize() int32 {
	return this.sessionSize
}

func (this *mgoBuilder) SetPoolSize(poolSize int32) MgoBuilderApi {
	this.poolSize = poolSize
	return this
}

func (this *mgoBuilder) GetPoolSize() int32 {
	return this.poolSize
}

func (this *mgoBuilder) GetHashName() string {
	return hash256(this.builderName)
}

func (this *mgoBuilder) SetIsDebug(isDebug bool) MgoBuilderApi {
	this.isDebug = isDebug
	return this
}

func (this *mgoBuilder) GetIsDebug() bool {
	return this.isDebug
}

func (this *mgoBuilder) SetLogDir(logDir string) MgoBuilderApi {
	this.logDir = logDir
	return this
}
func (this *mgoBuilder) GetLogDir() string {
	return this.logDir
}
