package MgoPool

/**
连接负载处理
*/
type loadBalance struct {

}

func newLoadBalance() *loadBalance {
	return &loadBalance{}
}

/**
负载均衡
轮循
*/
func (r *loadBalance)RoundRobin(cIndex, max int32) int32 {
	if max ==0 {return 0}
	return (cIndex+1)%max
}