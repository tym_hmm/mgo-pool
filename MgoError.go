package MgoPool

import (
	"bytes"
	"errors"
	"strconv"
)

var (
	//MGO_ERROR_BUILDER_EMPTY      = &MgoError{Code: 701, Message: "mgo builder empty", Err: errors.New("mgo builder empty")}
	//MGO_ERROR_BUILDER_NAME_EMPTY = &MgoError{Code: 702, Message: "mgo builder name can not be empty", Err: errors.New("mgo builder name can not be empty")}
	//MGO_ERROR_BUILDER_NOT_EXISTS = &MgoError{Code: 703, Message: "mgo builder not exists", Err: errors.New("mgo builder not exists")}
	//MGO_ERROR_CONNECTON_ERROR    = &MgoError{Code: 710, Message: "mgo connection error", Err: errors.New("mgo builder name can not be empty")}
	//MGO_EXEC_CALLBACK_ERROR      = &MgoError{Code: 720, Message: "mgo session call back error", Err: errors.New("mgo session call back error")}

	MGO_ERROR_BUILDER_EMPTY =errors.New("mgo builder empty")
	MGO_ERROR_BUILDER_NAME_EMPTY = errors.New("mgo builder name can not be empty")
	MGO_ERROR_BUILDER_NOT_EXISTS = errors.New("mgo builder not exists")
	MGO_ERROR_CONNECTION_ERROR = errors.New("mgo connection error")
	MGO_EXEC_CALLBACK_ERROR = errors.New("mgo session call back error")

)

type MgoError struct {
	Code    int
	Message string
	Err     error
}

func NewMgoError(code int, message string, err error) *MgoError {
	return &MgoError{
		Code:    code,
		Message: message,
		Err:     err,
	}
}

func (this MgoError) Error() string {
	var errBuf bytes.Buffer
	errBuf.WriteString("mgo error ")
	errBuf.WriteString("[")
	errBuf.WriteString("code:")
	errBuf.WriteString(strconv.Itoa(this.Code))
	errBuf.WriteString(", message: ")
	errBuf.WriteString(this.Message)
	errBuf.WriteString(", err: ")
	errBuf.WriteString(this.Err.Error())
	errBuf.WriteString("]")
	err := errBuf.String()
	errBuf.Reset()
	return err
}

func (this MgoError) RuntimeError() {
	panic(this.Error())
}
