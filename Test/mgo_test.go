package Test

import (
	"fmt"
	MgoPool "gitee.com/tym_hmm/mgo-pool"
	"gitee.com/tym_hmm/mgo-pool/Test/Field"
	"strconv"
	"sync"
	"testing"
)

func init() {
	mgoBuidler := MgoPool.NewMgoBuilder("default", "192.168.186.130:27017", "", "", "dataCenter")
	mgoBuidler.SetIsDebug(true)
	mgoBuidler.SetLogDir("/Log")
	mgoBuidler.SetSessionSize(5)
	mgoBuidler.SetPoolSize(10)
	MgoPool.MPool.SetBuilders(mgoBuidler)
	err := MgoPool.MPool.Connection()
	if err != nil {
		fmt.Println("err", err.Error())
	}
}

func TestGetSession(t *testing.T) {
	tModel := NewTestModel()
	//var wg sync.WaitGroup
	for {
		//wg.Add(1)
		go func() {
			//defer wg.Done()
			tModel.GetS()

		}()
	}
	//for i := 0; i < 100000; i++ {
	//
	//}
	//wg.Wait()
}

func TestMgo(t *testing.T) {
	var wg sync.WaitGroup
	number := 100000
	for i := 0; i < number; i++ {
		wg.Add(1)
		go func(num int) {
			defer wg.Done()
			data := &Field.FieldTest{
				Name: fmt.Sprintf("%s-%d", "test", num),
				Id:   strconv.Itoa(num),
			}
			tModel := NewTestModel()
			err := tModel.CreateRow(data)
			if err != nil {
				fmt.Println("ersss", err)
			} else {
				//fmt.Println("data", data)
			}
		}(i)
	}
	wg.Wait()
	fmt.Println("完成")
}

//func TestChange(t *testing.T) {
//	var poolSize int32 = 10
//	var changeSessionIndex int32 = 1
//	currentIndex := roundRobin(changeSessionIndex, poolSize)
//	currentNum := currentIndex - changeSessionIndex
//	atomic.AddInt32(&this.sessionIndex, currentNum)
//	sIndex := this.sessionIndex - 1
//	mSessions := this.sessions[builderNameHash][sIndex]
//}
//
///**
//负载均衡
//轮循
//*/
//func roundRobin(cIndex, max int32) int32 {
//	if max == 0 {
//		return 0
//	}
//	return (cIndex + 1) % max
//}
