package sub_table

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"time"
)

/**
任务记录
*/
type FieldNotifyTask struct {
	ShardId        string    `bson:"shard_id"`        // 分片键
	TradeNum       string    `bson:"trade_num"`       //任务标识
	UserIdentifier string    `bson:"user_identifier"` //用户标识
	TransTid       string    `bson:"trans_tid"`       //广告商交易号
	NotifyUrl      string    `bson:"notify_url"`      //推送地址
	Data           string    `bson:"data"`            //推送数据
	RetryCount     int       `bson:"retry_count"`     //尝试次数
	Status         int       `bson:"status"`          //发送状态
	StatusMsg      string    `bson:"status_msg"`      //状态报文
	SdkType        int       `bson:"sdk_type"`        //来源 聚合 直媒 推送
	PushType       int       `bson:"push_type"`       //推送类型
	Flag           string    `bson:"flag"`            //贴牌标识
	ReportTime     time.Time `bson:"report_time"`     //上报时间
	UpdateTime     time.Time `bson:"update_time"`     //更新时间
	CreateTime     time.Time `bson:"create_time"`     //创建时间
}

/**
获取集合名，
所有Field都需继承
*/
func (this FieldNotifyTask) CollectionName() string {
	return "data_ware_host_notify_task"
}

/**
获取分表集合名称
@param dataTimeInt string 分表日期 如:2022-05-11
*/
func (this *FieldNotifyTask) GetSeparateCollection(dateTimeInt string) string {
	return fmt.Sprintf("%s_%s", this.CollectionName(), dateTimeInt)
}

/**
索引键
*/
func (this *FieldNotifyTask) IndexKey() []mgo.Index {
	mgoIndex := []mgo.Index{
		{
			Key: []string{"$hashed:shard_id"},
		},
		{
			Key: []string{"trade_num"},
		},
		{
			Key: []string{"user_identifier"},
		},
		{
			Key: []string{"trans_tid"},
		},
		{
			Key: []string{"status"},
		},
		{
			Key: []string{"push_type"},
		},
		{
			Key: []string{"flag"},
		},
		{
			Key: []string{"sdk_type"},
		},
		{
			Key: []string{"report_time"},
		},
	}
	return mgoIndex
}

func (this *FieldNotifyTask) GetShareId() string        { return this.ShardId }
func (this *FieldNotifyTask) GetTradeNum() string       { return this.TradeNum }
func (this *FieldNotifyTask) GetUserIdentifier() string { return this.UserIdentifier }
func (this *FieldNotifyTask) GetTransTid() string       { return this.TransTid }
func (this *FieldNotifyTask) GetNotifyUrl() string      { return this.NotifyUrl }
func (this *FieldNotifyTask) GetData() string           { return this.Data }
func (this *FieldNotifyTask) GetRetryCount() int        { return this.RetryCount }
func (this *FieldNotifyTask) GetStatus() int            { return this.Status }
func (this *FieldNotifyTask) GetStatusMsg() string      { return this.StatusMsg }
func (this *FieldNotifyTask) GetSdkType() int           { return this.SdkType }
func (this *FieldNotifyTask) GetPushType() int          { return this.PushType }
func (this *FieldNotifyTask) GetFlag() string           { return this.Flag }
func (this *FieldNotifyTask) GetReportTime() time.Time  { return this.ReportTime }
func (this *FieldNotifyTask) GetUpdateTime() time.Time  { return this.UpdateTime }
func (this *FieldNotifyTask) GetCreateTime() time.Time  { return this.CreateTime }
