package Field

type FieldTest struct {
	Name string        `bson:"name"`
	Id   string        `bson:"id"`
}

/**
设置数据库
*/
func (this FieldTest) GetDataBase() string {
	return "dataCenter"
}
/**
设置集合命称
*/
func (this FieldTest) GetCollection() string {
	return "test"
}

func (this *FieldTest) GetName() string {
	return this.Name
}

func (this *FieldTest) GetId() string {
	return this.Id
}
