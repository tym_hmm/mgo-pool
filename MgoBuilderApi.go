package MgoPool

type MgoBuilderApi interface {
	//设置构建名称
	SetBuilderName(builderName string) MgoBuilderApi
	GetBuilderName() string

	//设置服务地址
	SetAddr(add string) MgoBuilderApi
	GetAddr() string

	//设置用户账号
	SetUser(user string) MgoBuilderApi
	GetUser() string

	//设置密码
	SetPwd(pwd string) MgoBuilderApi
	GetPwd() string

	//设置数据库
	SetDataBase(dataBase string) MgoBuilderApi
	GetDataBase() string

	/**
	设置session连接数
	*/
	SetSessionSize(sessionSize int32) MgoBuilderApi
	GetSessionSize() int32

	//设置session中每个连接池大小
	SetPoolSize(poolSize int32) MgoBuilderApi
	GetPoolSize() int32

	SetAuthDataBase(autDataBase string) MgoBuilderApi
	GetAuthDataBase() string

	//获取builder name hash
	GetHashName() string

	//是否为调试
	SetIsDebug(isDebug bool) MgoBuilderApi
	GetIsDebug() bool

	//设置日志保存目录
	SetLogDir(logDir string) MgoBuilderApi
	GetLogDir() string
}
